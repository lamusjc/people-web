# PeopleApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Instalar paquetes
Escriba `npm install` para instalar los paquetes necesarios para la app web.

## Iniciar App Web

Escriba `ng serve`. Navegue a `http://localhost:4200/`. Por defecto va a escuchar este puerto.

## Conectar con la API de Spring
Verifique que el puerto del servidor este escuchando en `http://localhost:8080/` en caso contrario, dirigase a `url.service.ts` y modificar la variable `this.url` con el puerto especificado.