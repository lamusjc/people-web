import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class UrlService {
    url: string;

    constructor() { 
        this.url = 'http://localhost:8080/api/';
    }

    getUrl() {
        return this.url;
    }

}
