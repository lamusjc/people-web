import { Injectable } from '@angular/core';
import { NetworkService } from './services/network-service.service';

@Injectable({
    providedIn: 'root'
})

export class AppService {
    constructor(private networkService: NetworkService) { }

    getPeople(form) {
        return this.networkService.get('people');
    }

    editPerson(id, data){
        return this.networkService.put('people/'+id, data);
    }

    getPeopleByLastname(apellido){
        return this.networkService.get('people/apellido/' + apellido);
    }

    newPerson(data){
        return this.networkService.post('people/create', data);
    }

    deletePerson(id){
        return this.networkService.delete('people/' + id);
    }

    deletePersonAll(){
        return this.networkService.delete('people/delete');
    }

}
