import { Component, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs/internal/Subject';
import { Router } from '@angular/router';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  form: FormGroup;
  data: any;
  resultsLength = 2;
  id_edit = '';

  public horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  public verticalPosition: MatSnackBarVerticalPosition = 'top';

  // Private
  private _unsubscribeAll: Subject<any>;

  displayedColumns: string[] = ['id', 'nombre', 'apellido', 'cedula', 'acciones'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _formBuilder: FormBuilder,
    private router: Router,
    private appService: AppService,
    private snackBar: MatSnackBar) {
    this.data = [];

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    // Reactive Form
    this.form = this._formBuilder.group({
      id: ['', Validators.nullValidator],
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      cedula: ['', Validators.required]
    });
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.obtenerPersonas();
  }

  obtenerPersonas(): void {
    this.form.get('nombre').setValue('');
    this.form.get('apellido').setValue('');
    this.form.get('cedula').setValue('');
    this.snackBar.open('Loading', 'INFO', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: 999999,
      panelClass: 'blue',
    });
    this.appService.getPeople(this.form.value).subscribe(res => {
      this.snackBar.open('Success', 'OK', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 3000,
        panelClass: 'green',
      });
      const rows = res.map((row) => {
        const json = {
          id: row.id,
          nombre: row.nombre,
          apellido: row.apellido,
          cedula: row.cedula
        };
        return json;
      });
      this.dataSource.data = rows;
      this.resultsLength = res.length;
    }, err => {
      this.snackBar.open('Error al listar personas', 'ERROR', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 3000,
        panelClass: 'red',
      });
    });
  }

  obtenerPersonaApellido(): void {
    this.snackBar.open('Loading', 'INFO', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: 999999,
      panelClass: 'blue',
    });
    this.appService.getPeopleByLastname(this.form.get('apellido').value).subscribe(res => {
      this.snackBar.open('Success', 'OK', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 3000,
        panelClass: 'green',
      });
      const rows = res.map((row) => {
        const json = {
          id: row.id,
          nombre: row.nombre,
          apellido: row.apellido,
          cedula: row.cedula
        };
        return json;
      });
      this.dataSource.data = rows;
      this.resultsLength = res.length;
    }, err => {
      this.snackBar.open('Error al listar personas', 'ERROR', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 3000,
        panelClass: 'red',
      });
    });
  }

  limpiar() {
    this.form.get('nombre').setValue('');
    this.form.get('apellido').setValue('');
    this.form.get('cedula').setValue('');
    this.obtenerPersonas();
  }

  eliminarPersona(id) {
    this.snackBar.open('Loading. . .', 'INFO', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: 99999,
      panelClass: 'blue',
    });

    this.appService.deletePerson(id).subscribe(res => {

    }, err => {
      if (err.status === 200) {
        this.snackBar.open('Persona eliminada!', 'OK', {
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
          duration: 1500,
          panelClass: 'green',
        });

        this.obtenerPersonas();
      } else {
        this.snackBar.open('Error al eliminar persona', 'ERROR', {
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
          duration: 3000,
          panelClass: 'red',
        });
      }
    });
  }
  agarrarValor(id) {
    this.id_edit = id;
  }
  editarPersona() {
    if (this.form.valid) {
      this.snackBar.open('Loading. . .', 'INFO', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 99999,
        panelClass: 'blue',
      });
      const formControl = this.form;

      const body = {
        nombre: formControl.get('nombre').value,
        apellido: formControl.get('apellido').value,
        cedula: formControl.get('cedula').value
      };

      this.appService.editPerson(this.id_edit, body).subscribe(res => {
        this.snackBar.open('Persona editada', 'OK', {
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
          duration: 1500,
          panelClass: 'green',
        });

        formControl.reset();
        this.id_edit = '';
        this.obtenerPersonas();
      }, err => {
        this.snackBar.open('Error al editar persona', 'ERROR', {
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
          duration: 3000,
          panelClass: 'red',
        });
      });
    }
  }
  eliminarTodos(id) {
    this.snackBar.open('Loading. . .', 'INFO', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: 99999,
      panelClass: 'blue',
    });

    this.appService.deletePersonAll().subscribe(res => {

    }, err => {
      if (err.status === 200) {
        this.snackBar.open('Persona eliminada!', 'OK', {
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
          duration: 1500,
          panelClass: 'green',
        });

        this.obtenerPersonas();
      } else {
        this.snackBar.open('Error al eliminar todos', 'ERROR', {
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
          duration: 3000,
          panelClass: 'red',
        });
      }
    });
  }

  nuevaPersona() {
    if (this.form.valid) {
      this.snackBar.open('Loading. . .', 'INFO', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 99999,
        panelClass: 'blue',
      });
      const formControl = this.form;

      const body = {
        nombre: formControl.get('nombre').value,
        apellido: formControl.get('apellido').value,
        cedula: formControl.get('cedula').value
      };

      this.appService.newPerson(body).subscribe(res => {
        this.snackBar.open(res.message, 'OK', {
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
          duration: 1500,
          panelClass: 'green',
        });

        formControl.reset();
        this.obtenerPersonas();
      }, err => {
        this.snackBar.open('Error al crear', 'ERROR', {
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
          duration: 3000,
          panelClass: 'red',
        });
      });
    }
  }

  ngOnDestroy(): void {

    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}